var express = require('express');
var app = express();

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


var auth = express.basicAuth('testUser', 'testPass');

app.get('/access', function(req, res) {
	console.log("Query recieved!! : " + req.query);
 	if(req.query.pass == "Honors2016")
 		res.sendfile('private/timesheets.html');
 	else
 		res.send("That password was incorrect please try again.");
});

app.use(express.static(__dirname + '/public'));

app.set('port', process.env.OPENSHIFT_NODEJS_PORT || process.env.PORT || 3002);
app.set('ip', process.env.OPENSHIFT_NODEJS_IP || "127.0.0.1");

app.listen(process.env.PORT || 3000);